/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ORDER {

    private int id;
    private Date date;
    private int qty;
    private double total;
    private ArrayList<OrderDetail> orderDetails;

    public ORDER(int id, Date date, int qty, double total, ArrayList<OrderDetail> orderDetail) {
        this.id = id;
        this.date = date;
        this.qty = qty;
        this.total = total;
        this.orderDetails = orderDetail;
    }

    public ORDER(Date date, int qty, double total, ArrayList<OrderDetail> orderDetail) {
        this.id = -1;
        this.date = date;
        this.qty = qty;
        this.total = total;
        this.orderDetails = orderDetail;
    }

    public ORDER() {
        this.id = -1;
        orderDetails = new ArrayList<>();
        qty = 0;
        total = 0;
        date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<OrderDetail> getOrderDetail() {
        return orderDetails;
    }

    public void setOrderDetail(ArrayList<OrderDetail> orderDetail) {
        this.orderDetails = orderDetail;
    }

    @Override
    public String toString() {
        return "ORDER{" + "id=" + id + ", date=" + date + ", qty=" + qty + ", total=" + total + '}';
    }

    public void addOrderDetail(OrderDetail orderDetail) {
        orderDetails.add(orderDetail);
        total += orderDetail.getTotal();
        qty += orderDetail.getQty();
    }

    public void addOrderDetail(Product product, String productName, double productPrice, int qty) {
        OrderDetail orderDetail = new OrderDetail(product, productName, productPrice, qty, this);
        this.addOrderDetail(orderDetail);
    }

    public void addOrderDetail(Product product, int qty) {
        OrderDetail orderDetail = new OrderDetail(product, product.getName(), product.getPrice(), qty, this);
        this.addOrderDetail(orderDetail);
    }

    public static ORDER fromRS(ResultSet rs) {
        ORDER order = new ORDER();
        try {
            order.setId(rs.getInt("order_id"));
            order.setQty(rs.getInt("order_qty"));
            order.setTotal(rs.getDouble("order_total"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            order.setDate(sdf.parse(rs.getString("order_date")));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(ORDER.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }

}
