/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poc;

import com.werapan.databaseproject.dao.OrderDao;
import com.werapan.databaseproject.model.ORDER;
import com.werapan.databaseproject.model.OrderDetail;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class TestOrder {

    public static void main(String[] args) {
        Product product1 = new Product(1, "A", 70);
        Product product2 = new Product(2, "B", 180);
        Product product3 = new Product(3, "C", 75);
        ORDER order = new ORDER();
        order.addOrderDetail(product1, 15);
        order.addOrderDetail(product2, 10);
        order.addOrderDetail(product3, 5);
        System.out.println(order);
        System.out.println(order.getOrderDetail());
        printReceipt(order);

        OrderDao orderDao = new OrderDao();
        ORDER newOrder = orderDao.save(order);
        System.out.println(newOrder);
//        System.out.println(orderDao.get(4));
        ORDER order1 = orderDao.get(newOrder.getId());
        printReceipt(order1);
    }

    static void printReceipt(ORDER order) {
        System.out.println("Order: " + order.getId());
        for (OrderDetail od : order.getOrderDetail()) {
            System.out.println(" " + od.getProductName() + " " + od.getQty() + " " + od.getProductPrice() + " " + od.getTotal());
        }
        System.out.println("Qty: " + order.getQty() + " Total: " + order.getTotal());
    }
}
